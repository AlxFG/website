const std = @import("std");
const httpz = @import("httpz");

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const allocator = gpa.allocator();

    var server = try httpz.Server().init(allocator, .{
        .port = 6942,
        .address = "0.0.0.0",
    });
    defer server.deinit();

    server.notFound(notFound);
    server.errorHandler(errorHandler);

    var router = server.router();
    router.get("/", index);
    router.get("/styles.css", css);
    router.get("/favicon.ico", favicon);
    router.get("/image.png", image);
    router.get("/kanata.kbd", kanata);

    std.log.info("listening on port 6942", .{});
    try server.listen();
}

fn notFound(_: *httpz.Request, res: *httpz.Response) !void {
    res.status = 404;
    const html = @embedFile("pages/404.html");
    res.body = html;
}

// note that the error handler return `void` and not `!void`
fn errorHandler(req: *httpz.Request, res: *httpz.Response, err: anyerror) void {
    res.status = 500;
    res.body = "Internal Server Error";
    std.log.warn("httpz: unhandled exception for request: {s}\nErr: {}", .{ req.url.raw, err });
}

fn index(req: *httpz.Request, res: *httpz.Response) !void {
    _ = req;
    const file = @embedFile("pages/index.html");
    res.body = file;
}

fn css(req: *httpz.Request, res: *httpz.Response) !void {
    _ = req;
    res.content_type = httpz.ContentType.CSS;
    const file = @embedFile("static/styles.css");
    res.body = file;
}

fn favicon(req: *httpz.Request, res: *httpz.Response) !void {
    _ = req;
    const file = @embedFile("static/favicon.ico");
    res.body = file;
}

fn image(req: *httpz.Request, res: *httpz.Response) !void {
    _ = req;
    const file = @embedFile("static/image.png");
    res.body = file;
}

fn kanata(req: *httpz.Request, res: *httpz.Response) !void {
    _ = req;
    const file = @embedFile("static/kanata.kbd");
    res.body = file;
}
